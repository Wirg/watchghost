Changelog
=========

Version 0.3.0
-------------

Released on 2019-07-31

Changes:

- test with Python 3.5, 3.6 and 3.7
- add a Code Of Conduct
- Tornado has been replaced by AioHttp
- reload of app with SIGHUP signal
- try to load config files only if present
- add a logger that stores events in InfluxDB
- add `get_info` option to Http
- add a `Whois` service
- use minified Javascrip and CSS assets
- add a favicon
- hide `info` watchers in dashboard
- sort watchers by tags instead of hosts in dashboard
- in watcher detail page, respect newlines in last result response
- use timezones with datetimes objects
- TOML has replaced YAML for configuration files.


Contributors:

- Arthur Vuillard
- Guillaume Ayoub
- grewn0uille
- Sam
- Sylvain Le Gal
- Lucien Deleu

Backward incompatible changes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As TOML replaced YAML for configuration files, you need to update those configuration files. 

We provide a tool called `watchghost-json2toml` that make the changes. The process to upgrade is the following:

- stop watchghost 0.2.2 or earlier
- install 0.3.0
- launch `watchghost-json2toml --config /path/to/your/config/dir`
- launch watchghost as usual

Version 0.2.2
-------------

Released on 2018-11-13

Changes:

- fix status for HTTP service when a error occurs before HTTP
- includes default configuration in package (fix #15)
- improve default configuration by addind a description to the HTTP watcher

Contributors:

- Arthur Vuillard

Version 0.2.1
-------------

Released on 2018-11-07

Changes:

- fix in packaging : license field is not meant to receive the full License but only its name

Contributors:

- Arthur Vuillard

Version 0.2.0
-------------

Released on 2018-11-07

Changes:

- web console is rewritten using Vue.js and Bootstrap 4
- Json API endpoints have been added
- fixes in documentation (it displayed Python instead of JSON as configuration examples)
- fix for TLS with Python 3.7
- packaging updates : use of setup.cfg for distributing and all configurations for development

Contributors:

- Arthur Vuillard
- Guillaume Ayoub
- Sylvain Le Gal

Version 0.1.0
-------------

Released on 2018-05-17

Initial release : Your invisible but loud monitoring pet

WatchGhost is a simple to install and simple to configure monitoring server. It is also a modern Python application making great use of asynchronous architecture.

Contributors:

- Arthur Vuillard
- Guillaume Ayoub
- Sam
- Sylvain Le Gal
- Lucie Anglade
- Pierre Charlet
